import axios from "axios";

export const getTodo = () => {
  return dispatch => {
    console.log("getTodo: ");
    dispatch(getTodoStarted());

    axios
      .get("https://jsonplaceholder.typicode.com/users/1/todos", {
        completed: false
      })
      .then(res => {
        dispatch(getTodoSuccess(res.data));
      })
      .catch(err => {
        dispatch(getTodoFailure(err.message));
      });
  };
};

const getTodoSuccess = todo => ({
  type: "GET_TODO_SUCCESS",
  payload: {
    ...todo
  }
});

const getTodoStarted = () => ({
  type: "GET_TODO_STARTED"
});

const getTodoFailure = error => ({
  type: "GET_TODO_FAILURE",
  payload: {
    error
  }
});

let nextTodoId = 0;
export const addTodo = title => ({
  type: "ADD_TODO",
  id: nextTodoId++,
  title
});

export const setVisibilityFilter = filter => ({
  type: "SET_VISIBILITY_FILTER",
  filter
});

export const toggleTodo = id => ({
  type: "TOGGLE_TODO",
  id
});

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE"
};
