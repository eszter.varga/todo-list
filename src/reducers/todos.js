const todos = (state = [], action) => {
  switch (action.type) {
    case "GET_TODO_STARTED": {
      return [...state];
    }

    case "GET_TODO_SUCCESS": {
      return [...state, ...Object.values(action.payload)];
    }

    case "GET_TODO_FAILURE": {
      return [...state];
    }

    case "ADD_TODO":
      return [
        ...state,
        {
          id: action.id,
          title: action.title,
          completed: false
        }
      ];

    case "TOGGLE_TODO":
      return state.map(
        todo =>
          todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    default:
      return state;
  }
};

export default todos;
