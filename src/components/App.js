import React, { Component } from "react";
import Footer from "./Footer";
import AddTodo from "../containers/AddTodo";
import { connect } from "react-redux";
import { getTodo } from "../actions";
import VisibleTodoList from "../containers/VisibleTodoList";
import "../static/css/stylesheet.css";

class App extends Component {
  componentDidMount() {
    this.props.dispatch(getTodo());
  }

  render() {
    return (
      <div className="todo-list">
        <h1>TODO LIST </h1>
        <AddTodo />
        <VisibleTodoList />
        <Footer />
      </div>
    );
  }
}

export default connect()(App);
